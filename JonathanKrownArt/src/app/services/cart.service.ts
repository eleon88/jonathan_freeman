import { Injectable } from '@angular/core';
import { CartItem } from '../cart/cart-item.model';

@Injectable({
  providedIn: 'root'
})
export class CartService{
  cartTotals : {subTotal: number, tax: number, discount: number, shipping: number, total: number};

  constructor() {
    this.cartTotals = {subTotal: 0, tax: 0, discount: 0, shipping: 0, total: 0};
   }

  private cart: CartItem[] = [

  ]

  addToCart(productId: string, name: string, type: string, image: string, size: string, quantity: number, price: number){
    let inCartProductId = this.cart.length > 0 ? this.cart[this.cart.length-1].inCartProductId+1 : 1;
    this.cart.push({inCartProductId: inCartProductId, productId: productId, name: name, type: type, image: image, size: size, quantity: quantity, price: price});
  }

  getCart(){
    return this.cart;
  }

  deleteFromCart(inCartProductId: number){
    const product = this.cart.find(p => {
      return p.inCartProductId === inCartProductId;
    });
    if(product){
      this.cart.splice(this.cart.map(p => p.inCartProductId).indexOf(inCartProductId), 1);
    }

    this.getTotals();
  }

  updateProductQuantity(inCartProductId: number, quantity: number, singleProductPrice: number) {
    const product = this.cart.find(p => {
      return p.inCartProductId === inCartProductId;
    });
    if(product){
      this.cart[this.cart.map(p => p.inCartProductId).indexOf(inCartProductId)].quantity = quantity;
      this.cart[this.cart.map(p => p.inCartProductId).indexOf(inCartProductId)].price = singleProductPrice*quantity;
    }
  }

  getTotals(){
    this.cartTotals = {subTotal: 0, tax: 0, discount: 0, shipping: 0, total: 0}
    for(let runningTotal of this.cart) {
      this.cartTotals.subTotal += runningTotal.price;
      this.cartTotals.tax += runningTotal.price*.08;
      this.cartTotals.discount = 0;
      this.cartTotals.shipping = 0;
    }
    this.cartTotals.total =
      this.cartTotals.subTotal +
      this.cartTotals.tax +
      this.cartTotals.discount +
      this.cartTotals.shipping;
      this.cartTotals.tax = Math.round(this.cartTotals.tax * 100) / 100;
    return this.cartTotals;
  }

  emptyCart(){
    this.cart.splice(0, this.cart.length)
  }
}
