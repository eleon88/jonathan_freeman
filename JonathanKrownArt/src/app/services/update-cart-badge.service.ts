import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpdateCartBadgeService {

  constructor() { }
  emmitProductQuantity = new Subject<number>();
}
