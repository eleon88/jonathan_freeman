import { Injectable } from '@angular/core';
import { CartItem } from '../cart/cart-item.model';
import { Order } from '../orders/order.model';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor() { }

  private orders: Order[] = [

  ]

  addOrder(customerName: string, email: string, phone: string, products: CartItem[], subTotal: number, discount: number, shipping: number, tax: number, total: number, isFullfilled: boolean){
    let orderId = this.orders.length > 0 ? this.orders[this.orders.length-1].orderId+1 : 1;
    this.orders.push({orderId: orderId, customerName: customerName, email: email, phone: phone, products: products, subTotal: subTotal, discount: discount, shipping: shipping, tax: tax, total: total, isFullfilled: isFullfilled});
  }

  getOrders(){
    return this.orders;
  }

  deleteOrder(orderId: number){
    const product = this.orders.find(p => {
      return p.orderId === orderId;
    });
    if(product){
      this.orders.splice(this.orders.map(p => p.orderId).indexOf(orderId), 1);
    }
  }

  updateIsFullfilled(orderId: number, isFullfilled: boolean) {
    const order = this.orders.find(p => {
      return p.orderId === orderId;
    });
    if(order){
      this.orders[this.orders.map(p => p.orderId).indexOf(orderId)].isFullfilled = !isFullfilled;
    }
  }
}
