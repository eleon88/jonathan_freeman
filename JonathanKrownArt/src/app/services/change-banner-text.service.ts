import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChangeBannerTextService {

  constructor() { }
  emmitNewTextValue = new Subject<string>();
}
