import { Injectable } from '@angular/core';
import { UserMessage } from "../user-messages/user-message.model";

@Injectable({
  providedIn: 'root'
})
export class UserMessagesService {

  constructor() { }

  private userMessages: UserMessage[] = [
    // new UserMessage(1,'test1','test1@test.com','I am a subject longer than 30 characters','first email test', true, 'tupac01'),
    // new UserMessage(2,'test2','test2@test.com','this is a test','second email test', false),
    // new UserMessage(3,'test3','test3@test.com','this is a test','third email test', true),
    // new UserMessage(4,'test4','test4@test.com','this is a test','fourth email test', false, 'eminem01')
    // new UserMessage(0,'','','','', false, '')
  ]

  getMessages(){
    return this.userMessages;
  }

  getMessage(messageId: number) {
    const userMessage = this.userMessages.find(p => {
      return p.messageId === messageId;
    });
    if(userMessage){
      return userMessage;
    } else {
      return {messageId: 0, name: '', email: '', subject: 0, message: '', isRead: false};
    }
  }

  updateIsRead(messageId: number, isRead: boolean) {
    const userMessage = this.userMessages.find(p => {
      return p.messageId === messageId;
    });
    if(userMessage){
      this.userMessages[this.userMessages.map(p => p.messageId).indexOf(messageId)].isRead = !isRead;
    }
  }

  addMessage(name: string, email: string, subject: string, message: string, isRead: boolean, artworkId?: string){
    let messageId = this.userMessages.length > 0 ? this.userMessages[this.userMessages.length-1].messageId+1 : 1;
    this.userMessages.push({messageId: messageId, name: name, email: email, subject: subject, message: message, isRead: isRead, artworkId: artworkId});
  }

  deleteMessage(messageId: number){
    const userMessage = this.userMessages.find(p => {
      return p.messageId === messageId;
    });

    if(userMessage){
      this.userMessages.splice(this.userMessages.map(p => p.messageId).indexOf(messageId), 1);
    }
  }
}
