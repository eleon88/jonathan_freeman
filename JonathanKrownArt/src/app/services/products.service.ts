import { Injectable } from '@angular/core';
import { Product } from "../gallery/product.model";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor() { }

  private products: Product[] = [
    new Product(1, 'Tupac', 'tupac01', 'assets/images/art1.JPG', 20, 'This is tupac', '18x20', [{printId: 1, size: '10x18', price: 3}, {printId: 2, size: '15x10', price: 3}, {printId: 3, size: '20x25', price: 3}], true, true),
    new Product(2, 'Eminem', 'eminem02', 'assets/images/art2.JPG', 20, 'This is eminem', '18x20', [{printId: 1, size: '15x10', price: 3}, {printId: 2, size: '20x25', price: 3}], true, false),
    new Product(3, 'Big E', 'big-e07', 'assets/images/art7.JPG', 20, 'This is big e', '18x20', [{printId: 1, size: '10x18', price: 3}, {printId: 2, size: '20x25', price: 3}], true, true),
    new Product(4, 'Snoop Dogg', 'snoop03', 'assets/images/art3.JPG', 20, 'This is snoop dogg', '18x20', [{printId: 1, size: '10x18', price: 3}, {printId: 2, size: '15x10', price: 3}], true, false),
    new Product(5, 'Ice Cube', 'icecube09', 'assets/images/art9.jpg', 20, 'This is ice cube', '18x20', [{printId: 1, size: '10x18', price: 3}, {printId: 2, size: '15x10', price: 3}, {printId: 3, size: '20x25', price: 3}], false, true),
    new Product(6, 'Dr Dre', 'dr-dre08', 'assets/images/art8.jpg', 20, 'This is dr dre', '18x20', [{printId: 1, size: '20x25', price: 3}], false, false),
    new Product(7, 'Microphone King Card', 'mic-king-card04', 'assets/images/art4.jpg', 15, 'This is a Microphone King Card', '18x20', [{printId: 1, size: '10x18', price: 3}], true, true),
    new Product(8, 'Michrophone Statue of Liberty', 'mic-liberty05', 'assets/images/art5.jpg', 20, 'This is a michrophone statue of liberty', '18x20', [{printId: 1, size: '10x18', price: 3}, {printId: 2, size: '15x10', price: 3}, {printId: 3, size: '20x25', price: 3}], true, false),
    new Product(9, 'Microphone king', 'mic-king06', 'assets/images/art6.png', 20, 'This is a microphone king', '18x20', [{printId: 1, size: '15x10', price: 3}], true, true)
  ]

  getProducts(){
    return this.products;
  }

  getProduct(id: number){
    const product = this.products.find(p => {
      return p.id === id;
    });
    if(product){
      return product;
    } else {
      return {id: 0, name:'', productId: '', image: '', price: 0, desc: '', size: '', prints: [], isOriginalAvailable:false, isPrintAvailable: false};
    }
  }

  addProduct(name: string, productId: string, image: string, price: number, desc: string, size: string, isOriginalAvailable: boolean, isPrintAvailable: boolean){
    let id = (this.products.length > 0 ? this.products[this.products.length-1].id+1 : 1);
    this.products.push({id: id, name: name, productId: productId, image: image, price: price, desc: desc, size: size, prints: [], isOriginalAvailable: isOriginalAvailable, isPrintAvailable: isPrintAvailable});
  }

  updateProduct(id: number, name: string, productId: string, price: number, desc: string, size: string, isOriginalAvailable: boolean, isPrintAvailable: boolean){
    const product = this.products.find(p => {
      return p.id === id;
    });
    if(product){
      this.products[this.products.map(p => p.id).indexOf(id)].name = name;
      this.products[this.products.map(p => p.id).indexOf(id)].productId = productId;
      this.products[this.products.map(p => p.id).indexOf(id)].price = price;
      this.products[this.products.map(p => p.id).indexOf(id)].desc = desc;
      this.products[this.products.map(p => p.id).indexOf(id)].size = size;
      this.products[this.products.map(p => p.id).indexOf(id)].isOriginalAvailable = isOriginalAvailable;
      this.products[this.products.map(p => p.id).indexOf(id)].isPrintAvailable = isPrintAvailable;
    }
  }

  updateIsOriginalAvailable(productId: string, isOriginalAvailable: boolean){
    const product = this.products.find(p => {
      return p.productId === productId;
    });
    if(product){
      this.products[this.products.map(p => p.productId).indexOf(productId)].isOriginalAvailable = isOriginalAvailable;
    }
  }

  addPrint(id: number, size: string, price: number){
    const product = this.products.find(p => {
      return p.id === id;
    });
    if(product){
      let printId = (product.prints.length > 0 ? product.prints[product.prints.length-1].printId+1 : 1);
      product.prints.push({printId: printId, size: size, price: price});
    }
    console.log(product?.prints)
  }

  deleteProduct(id: number){
    const product = this.products.find(p => {
      return p.id === id;
    });
    if(product){
      this.products.splice(this.products.map(p => p.id).indexOf(id), 1);
    }
  }

  deletePrint(id: number, printId: number){
    const product = this.products.find(p => {
      return p.id === id;
    });
    if(product){
      const print = product.prints.find(p => {
        return p.printId === printId;
      });
      if(print){
        product.prints.splice(product.prints.map(p => p.printId).indexOf(printId), 1);
      }
    }
  }
}
