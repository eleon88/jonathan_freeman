import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-confirm-checkout-dialog',
  templateUrl: './confirm-checkout-dialog.component.html',
  styleUrls: ['./confirm-checkout-dialog.component.css']
})
export class ConfirmCheckoutDialogComponent implements OnInit {
  validPhone = false;
  orderForm: FormGroup;

  constructor(){
    this.orderForm = new FormGroup({
      name : new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required, Validators.pattern("[0-9 ]{10}")])
    })
  }

  ngOnInit(): void{
  }

  onSubmit() {
    console.log(this.orderForm);
    console.log(this.orderForm.value);
  }
}
