import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { CartService } from '../services/cart.service';
import { OrdersService } from '../services/orders.service';
import { ProductsService } from '../services/products.service';
import { UpdateCartBadgeService } from '../services/update-cart-badge.service';
import { CartItem } from './cart-item.model';
import { ConfirmCheckoutDialogComponent } from './confirm-checkout-dialog/confirm-checkout-dialog.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  displayedColumns: string[] = ['product', 'description', 'quantity', 'price', 'delete'];
  cart : CartItem[] = [];
  cartTotals: {subTotal: number, tax: number, discount: number, shipping: number, total: number};
  cartStatus = 'Your cart is empty';
  @ViewChild(MatTable, {static: false}) table: MatTable<CartItem>;

  constructor(private cartService: CartService,
              private updateCartBadgeService: UpdateCartBadgeService,
              public dialog: MatDialog,
              private ordersService: OrdersService,
              private productService: ProductsService) { }

  ngOnInit() {
    this.cart = this.cartService.getCart();
    this.cartTotals = this.cartService.getTotals();
  }

  onDeleteProduct(inCartProductId: number, productId: string,  quantity: number){
    this.updateCartBadgeService.emmitProductQuantity.next(-quantity);
    this.cartService.deleteFromCart(inCartProductId);
    this.table.renderRows();
    this.cartTotals = this.cartService.getTotals();
    this.productService.updateIsOriginalAvailable(productId, true);
  }

  onChangeQuantity(inCartProductId: number, event: any) {
    let currentCartItem = this.cart.find(x => x.inCartProductId == inCartProductId);
    let singleProductPrice = 0;
    let quantityDiff = 0;
    if(currentCartItem) {
      singleProductPrice = currentCartItem.price / currentCartItem?.quantity;
      quantityDiff = event.target.value - currentCartItem?.quantity;
    }
    this.cartService.updateProductQuantity(inCartProductId, event.target.value, singleProductPrice);
    this.cartTotals = this.cartService.getTotals();
    this.updateCartBadgeService.emmitProductQuantity.next(quantityDiff);
  }

  onCheckout() {
    this.cart = this.cartService.getCart();
    this.cartTotals = this.cartService.getTotals();
    const dialogRef = this.dialog.open(ConfirmCheckoutDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.ordersService.addOrder(result.name, result.email, result.phone, this.cart.slice(), this.cartTotals.subTotal, this.cartTotals.discount, this.cartTotals.shipping, this.cartTotals.tax, this.cartTotals.total, false);
        this.cartService.emptyCart();
        this.updateCartBadgeService.emmitProductQuantity.next(-100);
        this.cartStatus = 'Your order has been placed';
      }
      console.log(`Dialog result: ${result.name}`);
    });
  }
}
