export class CartItem {
  constructor(
    public inCartProductId: number,
    public productId: string,
    public name: string,
    public type: string,
    public image: string,
    public size: string,
    public quantity: number,
    public price: number
  ){}
}
