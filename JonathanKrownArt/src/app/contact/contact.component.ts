import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ChangeBannerTextService } from '../services/change-banner-text.service';
import { UserMessagesService } from '../services/user-messages.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isEditable = false;

  constructor(private userMsg: UserMessagesService,
              private _formBuilder: FormBuilder,
              private router: Router,
              private changeBannerText: ChangeBannerTextService) {
    this.firstFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required],
      emailCtrl: [null, [Validators.required, Validators.email]]
    });
    this.secondFormGroup = this._formBuilder.group({
      subjectCtrl: ['', Validators.required],
      artworkIdCtrl: [null],
      messageCtrl: ['', Validators.required]
    });
  }

  ngOnInit() {

  }

  onSendMessage(){
    this.userMsg.addMessage(
      this.firstFormGroup.value.nameCtrl,
      this.firstFormGroup.value.emailCtrl,
      this.secondFormGroup.value.subjectCtrl,
      this.secondFormGroup.value.messageCtrl,
      false,
      this.secondFormGroup.value.artworkIdCtrl);
  }

  returnHome(){
    this.changeBannerText.emmitNewTextValue.next('Jonathan Krown Art');
    this.router.navigate(['/']);
  }
}
