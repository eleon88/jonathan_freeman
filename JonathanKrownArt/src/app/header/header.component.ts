import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChangeBannerTextService } from '../services/change-banner-text.service';
import { UpdateCartBadgeService } from '../services/update-cart-badge.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  logoImgPath = "assets/images/logo.png";
  facebookHref = 'https://www.facebook.com/jonathan.freeman.927';
  facebookImgSrc = 'assets/images/icon_facebook.png';
  instagramHref = 'https://www.instagram.com/jonathankrownart/';
  instagramImgSrc = 'assets/images/icon_instagram.png';
  linkedinImgSrc = 'assets/images/icon_linkedin.png';
  linkedinHref = 'https://www.linkedin.com/';
  cartQuantity = 0;
  selected : any;
  hidden = true;

  constructor(private router: Router,
              private updateCartBadgeService: UpdateCartBadgeService,
              private changeBannerText: ChangeBannerTextService) { }

  ngOnInit() {
    this.updateCartBadgeService.emmitProductQuantity.subscribe((cartQuantity) =>{
      if(cartQuantity === -100) {
        this.cartQuantity = 0;
      } else {
        this.cartQuantity += cartQuantity;
      }
      if(this.cartQuantity > 0){
        this.hidden = false;
      } else {
        this.hidden = true;
      }
    });
    console.log(this.cartQuantity)
  }

  homePage(){
    this.selected = '';
    this.router.navigate(['/']);
    this.changeBannerText.emmitNewTextValue.next('Jonathan Krown Art');
  }

  navigateToPage(page: string, bannerText: string){
    this.changeBannerText.emmitNewTextValue.next(bannerText);
    this.router.navigate([`/${page}`]);
  }
}
