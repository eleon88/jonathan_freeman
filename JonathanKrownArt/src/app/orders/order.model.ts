import { CartItem } from '../cart/cart-item.model';

export class Order {
  constructor(public orderId: number,
              public customerName: string,
              public email: string,
              public phone: string,
              public products: CartItem[],
              public subTotal: number,
              public discount: number,
              public shipping: number,
              public tax: number,
              public total: number,
              public isFullfilled: boolean) {}
}
