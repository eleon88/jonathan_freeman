import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrdersService } from '../services/orders.service';
import { DeleteOrderDialogComponent } from './delete-order-dialog/delete-order-dialog.component';
import { Order } from './order.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  displayedColumns: string[] = ['product','order', 'price', 'totals'];
  orders: Order[] = [];
  panelOpenState = false;

  constructor(private ordersService: OrdersService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.orders = this.ordersService.getOrders();
  }

  onIsFulfilled(orderId: number, isFullfilled: boolean){
    this.ordersService.updateIsFullfilled(orderId, isFullfilled);
  }

  openDialog(orderId: number) {
    const dialogRef = this.dialog.open(DeleteOrderDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.ordersService.deleteOrder(orderId);
      }
      console.log(`Dialog result: ${result}`);
    });
  }
}
