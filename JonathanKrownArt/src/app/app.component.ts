import { Component, OnInit } from '@angular/core';
import { ChangeBannerTextService } from './services/change-banner-text.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  bannerText = "Jonathan Krown Art";
  logoImgPath = "assets/images/logo.png";

  constructor(private changeBannerText: ChangeBannerTextService){}

  ngOnInit() {
    this.changeBannerText.emmitNewTextValue.subscribe((bannerText) => {
      this.bannerText = bannerText;
      console.log(bannerText);
    });
  }
}
