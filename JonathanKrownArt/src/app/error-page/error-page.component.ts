import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-page',
  template: '<div class="mat-display-3">Page not found</div>',
  styles: [`
  div {
    background-color: #d6d6d6;
    padding: 20px;
    color: #a7181b;
    text-align: center;
  }`]
})
export class ErrorPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
