import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserMessagesService } from '../services/user-messages.service';
import { UserMessage } from '../user-messages/user-message.model';
import { DeleteMessageDialogComponent } from './delete-message-dialog/delete-message-dialog.component';

@Component({
  selector: 'app-user-messages',
  templateUrl: './user-messages.component.html',
  styleUrls: ['./user-messages.component.css']
})
export class UserMessagesComponent implements OnInit {
  userMsgs: UserMessage[] = [];
  panelOpenState = false;

  constructor(private userMessagesService: UserMessagesService, public dialog: MatDialog) { }

  ngOnInit() {
    this.userMsgs = this.userMessagesService.getMessages();
  }

  openDialog(messageId: number) {
    const dialogRef = this.dialog.open(DeleteMessageDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.userMessagesService.deleteMessage(messageId);
      }
      console.log(`Dialog result: ${result}`);
    });
  }

  onMarkReadUnread(messageId: number, isRead: boolean){
    this.userMessagesService.updateIsRead(messageId, isRead);
  }

  onDelete(messageId: number){
    this.userMessagesService.deleteMessage(messageId);
  }
}
