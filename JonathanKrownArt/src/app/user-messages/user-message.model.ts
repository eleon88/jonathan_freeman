export class UserMessage {
  constructor(
    public messageId: number,
    public name: string,
    public email: string,
    public subject: string,
    public message: string,
    public isRead: boolean,
    public artworkId?: string){}
}
