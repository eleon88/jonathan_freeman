import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChangeBannerTextService } from 'src/app/services/change-banner-text.service';
import { ProductsService } from 'src/app/services/products.service';
import { Print } from '../print.model';
import { Product } from '../product.model';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  product : Product = new Product(0, '', '', '', 0, '', '', [], false, false);
  id = 0;
  prints: Print[] = [];
  editProductForm: FormGroup;
  isNewProduct = false;
  productId = '';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private changeBannerText: ChangeBannerTextService,
              private productService: ProductsService) {
                this.route.params.subscribe((params) => {
                  this.product = this.productService.getProduct(+params['id']);
                  this.id = params['id'];
                  this.productId = this.product.productId;
                });
                this.prints = this.product.prints;
                this.editProductForm = new FormGroup({
                  'id': new FormControl(this.product.id),
                  'name': new FormControl(this.product.name, Validators.required),
                  'productId': new FormControl(this.product.productId, [Validators.required, this.existingUniqueProductId.bind(this)]),
                  'image': new FormControl(this.product.image),
                  'price': new FormControl(this.product.price, Validators.required),
                  'desc': new FormControl(this.product.desc, Validators.required),
                  'size': new FormControl(this.product.size, Validators.required),
                  'printSize': new FormControl(''),
                  'printPrice': new FormControl(''),
                  'isOriginalAvailable': new FormControl(this.product.isOriginalAvailable, Validators.required),
                  'isPrintAvailable': new FormControl(this.product.isPrintAvailable, Validators.required)
                })
              }

  ngOnInit() {
  }

  onSubmit() {
    if(!this.isNewProduct) {
      this.productService.updateProduct(
        this.editProductForm.value.id,
        this.editProductForm.value.name,
        this.editProductForm.value.productId,
        this.editProductForm.value.price,
        this.editProductForm.value.desc,
        this.editProductForm.value.size,
        this.editProductForm.value.isOriginalAvailable,
        this.editProductForm.value.isPrintAvailable
        )
        this.router.navigate(['product', this.id]);
    } else {
      this.productService.addProduct(
        this.editProductForm.value.name,
        this.editProductForm.value.productId,
        this.editProductForm.value.image,
        this.editProductForm.value.price,
        this.editProductForm.value.desc,
        this.editProductForm.value.size,
        this.editProductForm.value.isOriginalAvailable,
        this.editProductForm.value.isPrintAvailable
        )
        this.router.navigate(['']);
    }
    console.log(this.editProductForm.valid)
  }

  onDeleteProduct() {
    this.productService.deleteProduct(this.product.id);
    this.router.navigate(['']);
  }

  onAddPrint(id: number) {
    this.productService.addPrint(id, this.editProductForm.value.printSize, this.editProductForm.value.printPrice);
  }

  onDeletePrint(id: number, printId: number) {
    this.productService.deletePrint(id, printId);
  }

  onAddNewProduct() {
    this.product = new Product(0, '', '', '', 0, '', '', [], false, false);
    this.editProductForm = new FormGroup({
      'id': new FormControl(null),
      'name': new FormControl(null, Validators.required),
      'productId': new FormControl(null, [Validators.required, this.newUniqueProductId.bind(this)]),
      'image': new FormControl(null),
      'price': new FormControl(null, Validators.required),
      'desc': new FormControl(null, Validators.required),
      'size': new FormControl(null, Validators.required),
      'prints': new FormControl(null),
      'printSize': new FormControl(null),
      'printPrice': new FormControl(null),
      'isOriginalAvailable': new FormControl(null, Validators.required),
      'isPrintAvailable': new FormControl(null, Validators.required)
    })
    this.changeBannerText.emmitNewTextValue.next('New Product Details');
    this.isNewProduct = true;
  }

  newUniqueProductId(control: FormControl) : {[s: string]: boolean} | null {
    let products = this.productService.getProducts();
    const product = products.find(p => {
      return p.productId === control.value;
    });
    if(product){
      console.log(product.productId);
      return {'duplicateProductId': true};
    }
    return null;
  }

  existingUniqueProductId(control: FormControl) : {[s: string]: boolean} | null {
    let products = this.productService.getProducts();
    const product = products.find(p => {
      return p.productId === control.value;
    });
    if(product && this.productId !== control.value){
      console.log(product.productId);
      return {'duplicateProductId': true};
    }
    return null;
  }

  onCancelNewProduct() {
    this.router.navigate(['product', this.id]);
  }
}
