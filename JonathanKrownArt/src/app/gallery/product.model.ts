import { Print } from "../gallery/print.model";

export class Product {
  constructor(
    public id: number,
    public name: string,
    public productId: string,
    public image: string,
    public price: number,
    public desc: string,
    public size: string,
    public prints: Print[],
    public isOriginalAvailable: boolean,
    public isPrintAvailable: boolean
  ){}
}
