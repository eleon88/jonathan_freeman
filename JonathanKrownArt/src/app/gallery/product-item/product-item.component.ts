import { Component, DoCheck, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from 'src/app/services/cart.service';
import { ChangeBannerTextService } from 'src/app/services/change-banner-text.service';
import { ProductsService } from 'src/app/services/products.service';
import { UpdateCartBadgeService } from 'src/app/services/update-cart-badge.service';
import { Print } from '../print.model';
import { Product } from '../product.model';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit, DoCheck {
  product : Product = new Product(0, '', '', '', 0, '', '', [], false, false);
  id = 0;
  isDisabled = false;
  prints: Print[] = [];
  isPrintAvailable = true;
  isOriginalAvailable = true;
  TotalPrice = 0;
  productForm: FormGroup;
  typeControl = new FormControl('none');
  quantityControl = new FormControl(1);
  printSizeControl = new FormControl('');
  printPrice = 0;

  constructor(private route: ActivatedRoute,
              private updateCartBadgeService: UpdateCartBadgeService,
              private _snackBar: MatSnackBar,
              private cartService: CartService,
              private productService: ProductsService,
              private changeBannerText: ChangeBannerTextService,
              private router: Router,
              fb: FormBuilder) {
                this.productForm = fb.group({
                  type: this.typeControl,
                  printSize: this.printSizeControl,
                quantity: this.quantityControl
                })
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.product = this.productService.getProduct(+params['id']);
      console.log(this.product)
      this.id = params['id'];
    });
    this.isPrintAvailable = this.product.isPrintAvailable;
    this.isOriginalAvailable = this.product.isOriginalAvailable;
    this.prints = this.product.prints;
    this.TotalPrice = this.product.price;
  }

  ngDoCheck(){
    if(this.typeControl.value == 'original'){
      this.TotalPrice = this.product.price;
    }
    else {
      this.TotalPrice = this.printPrice*this.quantityControl.value;
    }
  }

  onSetPrintPrice(price: number){
    this.printPrice = price;
  }

  onEdit(id: number) {
    this.changeBannerText.emmitNewTextValue.next(`Edit: ${this.product.productId}`);
    this.router.navigate(['product', id, 'edit']);
  }

  onSubmit(){
    this.cartService.addToCart(this.product.productId,
                                this.product.name,
                                this.productForm.value.type,
                                this.product.image,
                                this.productForm.value.type === 'original' ? this.product.size : this.productForm.value.printSize,
                                this.productForm.value.quantity,
                                this.TotalPrice);
    if(this.productForm.value.type) {
      this.productService.updateIsOriginalAvailable(this.product.productId, false);
      this.isOriginalAvailable = false;
    }
    this._snackBar.open(`${this.product.name} has been added to your cart`,'close', {
      duration: 4000,
      verticalPosition: 'top'
    });
    this.updateCartBadgeService.emmitProductQuantity.next(this.productForm.value.type === 'print' ? +this.productForm.value.quantity : 1);
    this.productForm.reset();
  }
}
