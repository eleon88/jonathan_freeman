import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChangeBannerTextService } from '../services/change-banner-text.service';
import { ProductsService } from '../services/products.service';
import { Product } from './product.model';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  products : Product[] = [];

  constructor(private productService: ProductsService,
              private router: Router,
              private changeBannerText: ChangeBannerTextService) {}

  ngOnInit() {
    this.products = this.productService.getProducts();
  }

  onLoadProduct(id: number, name: string) {
    this.changeBannerText.emmitNewTextValue.next(`Art: ${name}`);
    this.router.navigate(['product', id]);
  }
}
