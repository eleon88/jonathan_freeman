import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { CartComponent } from './cart/cart.component';
import { ContactComponent } from './contact/contact.component';
import { GridColsDirective } from './directives/grid-cols.directive';
import { ErrorPageComponent } from './error-page/error-page.component';
import { FooterComponent } from './footer/footer.component';
import { EditProductComponent } from './gallery/edit-product/edit-product.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ProductItemComponent } from './gallery/product-item/product-item.component';
import { HeaderComponent } from './header/header.component';
import { MaterialModule } from './material/material.module';
import { UserMessagesComponent } from './user-messages/user-messages.component';
import { DeleteMessageDialogComponent } from './user-messages/delete-message-dialog/delete-message-dialog.component';
import { OrdersComponent } from './orders/orders.component';
import { ConfirmCheckoutDialogComponent } from './cart/confirm-checkout-dialog/confirm-checkout-dialog.component';
import { DeleteOrderDialogComponent } from './orders/delete-order-dialog/delete-order-dialog.component';
import { DisableControlDirective } from './directives/disable-control.directive';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    GalleryComponent,
    CartComponent,
    ContactComponent,
    ErrorPageComponent,
    ProductItemComponent,
    EditProductComponent,
    GridColsDirective,
    AuthenticationComponent,
    UserMessagesComponent,
    DeleteMessageDialogComponent,
    OrdersComponent,
    ConfirmCheckoutDialogComponent,
    DeleteOrderDialogComponent,
    DisableControlDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
