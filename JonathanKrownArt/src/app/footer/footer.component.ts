import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChangeBannerTextService } from '../services/change-banner-text.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  logoImgPath = "assets/images/logo.png";
  facebookHref = 'https://www.facebook.com/jonathan.freeman.927';
  facebookImgSrc = 'assets/images/icon_facebook.png';
  instagramHref = 'https://www.instagram.com/jonathankrownart/';
  instagramImgSrc = 'assets/images/icon_instagram.png';
  linkedinImgSrc = 'assets/images/icon_linkedin.png';
  linkedinHref = 'https://www.linkedin.com/';

  constructor(private changeBannerText: ChangeBannerTextService,
              private router: Router) { }

  ngOnInit(): void {
  }

  homePage(){
    this.changeBannerText.emmitNewTextValue.next('Jonathan Krown Art');
    this.router.navigate(['/']);
  }
}
