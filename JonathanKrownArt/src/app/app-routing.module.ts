import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from './authentication/authentication.component';
import { CartComponent } from './cart/cart.component';
import { ContactComponent } from './contact/contact.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { EditProductComponent } from './gallery/edit-product/edit-product.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ProductItemComponent } from './gallery/product-item/product-item.component';
import { OrdersComponent } from './orders/orders.component';
import { UserMessagesComponent } from './user-messages/user-messages.component';

const routes: Routes = [
  { path: '', component: GalleryComponent},
  { path: 'product/:id', component: ProductItemComponent},
  { path: 'cart', component: CartComponent},
  { path: 'login', component: AuthenticationComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'userMessages', component: UserMessagesComponent},
  { path: 'orders', component: OrdersComponent},
  { path: 'product/:id/edit', component: EditProductComponent},
  { path: 'not-found', component: ErrorPageComponent},
  { path: '**', redirectTo: '/not-found', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
